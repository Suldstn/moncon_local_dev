# -*- coding: utf-8 -*-

import base64
from datetime import datetime, timedelta
import decimal
import logging
import pytz
import time

import xlrd

from odoo import models, fields, api, _
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


_logger = logging.getLogger(__file__)


class HrAttendanceCalculate(models.TransientModel):
    _inherit = "hr.attendance.calculate"
            
    @api.multi
    def calculate_employee_attendances(self, employee, date_from_att=False, date_to_att=False):
        if employee.contract_id and not employee.contract_id.hour_balance_calculate_type == 'payroll_by_roster':
            super(HrAttendanceCalculate, self).calculate_employee_attendances(employee, date_from_att=False, date_to_att=False)
        else:
            date_from = date_from_att
            date_to = date_to_att
            
            if not date_from:
                date_from = datetime.strptime('%s 00:00:00' % self.date_from, DEFAULT_SERVER_DATETIME_FORMAT)
            if not date_to:
                date_to = datetime.strptime('%s 23:59:59' % self.date_to, DEFAULT_SERVER_DATETIME_FORMAT)
            
            raw_data = self.env['hr.attendance.raw.data'].search([('employee_id', '=', employee.sudo().id), ('date', '>=', str(date_from)[0:10]), ('date', '<=', str(date_to)[0:10])])
            attendances_days = [get_day_like_display(att.date, self.env.user) for att in raw_data]
            
            employee_attendances = self.env['hr.attendance'].sudo().search([('employee_id', '=', employee.sudo().id)])
            roster_emp_lines = self.env['hr.roster.employee.line'].search([('hr_shift_id', '!=', False), ('employee_id', '=', employee.id), ('date', '>=', str(date_from)[0:10]), ('date', '<=', str(date_to)[0:10])]).filtered(lambda x: x not in employee_attendances.mapped('roster_emp_line'))
            
            if not roster_emp_lines:
                return
            
            def get_raw_data(date, attendances):
                for att in attendances:
                    if att.date == date:
                        return att
                return False
            
            def get_end_date(date, shift):
                end_date = datetime.strptime(date, "%Y-%m-%d")
                if shift.is_day:
                    end_date += relativedelta(days=shift.shift_day)
                return get_day_by_user_timezone(end_date, self.env.user)
            
            def can_divorce(time_from, time_to):
                # Ээлжийн гарах/орох хуруу уншуулах боломжит цагийн интервалыг тооцоход
                # Тухайн ээлж Эхлэх/Дуусах цагийн аль нэг нь 00:00 цаг үед интервалын цагийг өмнөх өдрийн боломжит цагаас эхлэн тооцох эсэхийг тооцоолж буцаах функц
                # Ж/нь: Ээлжийн ----> Дуусах цаг 00:00 
                #                     Гарвал зохих цаг 23:00-01:00 бол 23:00 цаг нь өмнөх өдрөөс тооцоолох ёстой гэдгийг тооцож буцаана.
                time_from = int(time_from)
                time_to = int(time_to)
                 
                can_divorce = False
                hour = 1
                while hour < 24:
                    if time_from == abs(time_to - hour):
                        can_divorce = True
                        break
                    hour += 1
                    
                return can_divorce
                                
            for roster_emp_line in roster_emp_lines:
                shift = roster_emp_line.sudo().hr_shift_id
                # Ээлж дэх эхлэх цаг/дуусах/орвол зохих/гарвал зохих цагууд NULL утгатай байвал тооцоолол хийх боломжгүй
                if not (shift.time_from is not None and shift.time_to  is not None and shift.in_date_from  is not None and shift.in_date_to  is not None and shift.out_date_from  is not None and shift.out_date_to  is not None):
                    continue
                
                # Ирц тооцох боломжит эхлэх-from_time/дуусах-to_time огноонуудыг авах /Өдөр дамжин ажиллах бол гарвал зохих цагыг тооцохдоо дамжих өдрөөс хамааруулан тооцох/
                from_time = datetime.combine(get_day_by_user_timezone(datetime.strptime(roster_emp_line.date, "%Y-%m-%d"), self.env.user), datetime.min.time())
                to_time = datetime.combine(get_end_date(roster_emp_line.date, shift), datetime.min.time())

                # Ээлжийн гарах/орох хуруу уншуулах боломжит цагийн интервалыг тооцож эхлэх-from_time/дуусах-to_time огноог оноох
                if shift.in_date_from > shift.time_from and can_divorce(shift.in_date_from, shift.time_from):
                    from_time += relativedelta(days=-1)
                if shift.out_date_from > shift.time_to and can_divorce(shift.out_date_from, shift.time_to):
                    to_time += relativedelta(days=-1)
                    
                # Орвол/Гарвал зохих цагуудын интервал дах эхлэх цаг нь дуусах цагаас их бол дуусах огноог тооцохдоо дараах өдрөөс тооцох
                check_out_date_to_date = to_time
                if shift.out_date_from > shift.out_date_to:
                    check_out_date_to_date += relativedelta(days=1) 
                
                check_in_start_date_range = get_day_like_display_from_floattime(from_time, shift.in_date_from, self.env.user)
                check_out_end_date_range = get_day_like_display_from_floattime(check_out_date_to_date, shift.out_date_to, self.env.user)
                
                available_check_in, available_check_out = self.get_available_min_max_date(check_in_start_date_range, check_out_end_date_range, attendances_days)
                
                if available_check_in or available_check_out:
                    from_check_in = True
                    
                    if not available_check_out:
                        available_check_out = available_check_in + relativedelta(seconds=1)
                        
                    if not available_check_in:
                        from_check_in = False
                        available_check_in = available_check_out + relativedelta(seconds=-1)
                    
                    # check day attendance
                    if (available_check_in > available_check_out):
                        continue
                    
                    day_attendance = self.env['hr.attendance'].create({
                        'employee_id': employee.id,
                        'check_in': str(get_display_day_to_user_day(available_check_in, self.env.user)) if available_check_in else False,
                        'check_out': str(get_display_day_to_user_day(available_check_out, self.env.user)) if available_check_out else False,
                        'in_device_name': self.get_device_name1(available_check_in, raw_data),
                        'out_device_name': self.get_device_name1(available_check_out, raw_data),
                        'roster_emp_line': roster_emp_line.sudo().id
                    })
                    
                    check_in_raw_data = get_raw_data(available_check_in, raw_data)
                    check_out_raw_data = get_raw_data(available_check_out, raw_data)
                    if check_in_raw_data:
                        check_in_raw_data.write({'attendance_id': day_attendance.id})
                    if check_out_raw_data:
                        check_out_raw_data.write({'attendance_id': day_attendance.id})