# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Moncon Human Resource",
    'version': '10.0.1.0',
    'depends': [
        'l10n_mn_hr',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian HR Modules',
    'description': """
         Moncon HR employee
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/moncon_hr_worked_project_views.xml',
        'data/emp_birthday_reminder_cron.xml',
        'data/emp_birthday_reminder_template.xml',
        'data/emp_birthday_greeting_sender_template.xml',
        'views/moncon_hr_department_tag_views.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
