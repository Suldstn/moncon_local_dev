# -*- coding: utf-8 -*-
from odoo import models, api


class MonconCashExpense(models.Model):
    _inherit = 'hr.expense.cash'

    @api.multi
    def paid_button_function(self):
        self.write({'state': 'done'})
        self._line_state_update('done')

    @api.multi
    def cancel_button_function(self):
        self.write({'state': 'waiting_payment'})
        self._line_state_update('waiting_payment')
