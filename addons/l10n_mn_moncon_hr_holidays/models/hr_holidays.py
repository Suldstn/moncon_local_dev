# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError
from odoo import tools
from datetime import timedelta, datetime
import pytz

class HrHolidays(models.Model):

    _inherit = "hr.holidays"

    @api.multi
    def action_send(self):
        if self.type == 'remove' and not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days'):
            tz = get_user_timezone(self.env.user)
            date_from = pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
            today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            str_start = today - timedelta(today.weekday())
            if str_start.date() > date_from.date():
                raise UserError(u'Тухайн долоо хонгын амралт чөлөөны хугацааны хязгаар хэтэрсэн байна!!!')
        return super(HrHolidays, self).action_send()

    @api.multi
    def action_approve(self):
        if self.type == 'remove' and not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days'):
            tz = get_user_timezone(self.env.user)
            date_from = pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
            today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            str_start = today - timedelta(today.weekday())
            if str_start.date() > date_from.date():
                if today.weekday() == 0:
                    if today.hour >=18:
                        raise UserError(u'Таны батлах долоо хоног хэтэрсэн байна!!!')
                    else:
                        str_start = today - timedelta(days=7)
                        if str_start.date() > date_from.date():
                            raise UserError(u'Таны батлах долоо хоног хэтэрсэн байна!!!')
                else:
                    raise UserError(u'Таны батлах долоо хоног хэтэрсэн байна!!!')
        return super(HrHolidays, self).action_approve()

    nohon_holidays_type = fields.Selection([('nohon_add', u'Нөхөн амрах хоног хуримтлуулах'),
                                            ('nohon_remove', u'Нөхөн амрах хоног биеэр эдлэх')], default='nohon_add')
    add_date_from = fields.Datetime('Date from')
    add_date_to = fields.Datetime('Date To')
    check_in = fields.Datetime(string="Check In", compute='_compute_check_in')
    check_out = fields.Datetime(string="Check Out", compute='_compute_check_in')

    @api.multi
    @api.onchange('add_date_from')
    def _compute_check_in(self):
        for obj in self:
            if obj.add_date_from:
                start = datetime.strptime(obj.add_date_from, '%Y-%m-%d %H:%M:%S').date()
                attendances = self.env['hr.attendance.download'].get_attendances_of_day(obj.employee_id, start)
                for attendance in attendances:
                    obj.check_in = attendance.check_in
                    obj.check_out = attendance.check_out

    @api.model
    def create(self, vals):
        res = super(HrHolidays, self).create(vals)
        detail = self.env['hr.attendance.detail'].search([('report_datetime', '=', res.date_from)])
        if not detail and res.date_from:
            self.env['hr.attendance.detail'].create({'report_datetime': res.date_from, 'name': 'Holidays'})

        detailtype = self.env['hr.attendance.detail'].search([('report_datetime', '=', res.add_date_from)])
        if not detailtype and res.add_date_from:
            self.env['hr.attendance.detail'].create({'report_datetime': res.add_date_from, 'name': 'Holidays'})
        return res

    @api.onchange('nohon_holidays_type')
    def _onchange_nohon_holidays_type(self):
        config_hr_holidays = self.env['hr.holidays.status'].search([('is_nohon_amrakh', '=', True)], limit=1)
        for obj in self:
            obj.holiday_status_id = config_hr_holidays.id
            if self.nohon_holidays_type == 'nohon_add':
                obj.type = 'add'
            else:
                obj.type = 'remove'

    @api.onchange('add_date_from')
    def _onchange_add_date_from(self):
        date_from = self.add_date_from
        date_to = self.add_date_to
        if (date_to and date_from) and (date_from <= date_to):
            if self.nohon_holidays_type == 'nohon_add':
                from_dt = fields.Datetime.from_string(date_from)
                to_dt = fields.Datetime.from_string(date_to)
                time_delta = to_dt - from_dt
                total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600
                self.number_of_hours_temp = total_hours
                self.number_of_days_temp = total_hours / 8

    @api.onchange('number_of_hours_temp')
    def _onchange_number_of_hours_temp(self):
        super(HrHolidays, self)._onchange_number_of_hours_temp()
        if self.nohon_holidays_type == 'nohon_add':
            self.number_of_days_temp = self.number_of_hours_temp / 8

    @api.onchange('add_date_to')
    def _onchange_add_to_date_from(self):
        date_from = self.add_date_from
        date_to = self.add_date_to
        if (date_to and date_from) and (date_from <= date_to):
            if self.nohon_holidays_type == 'nohon_add':
                from_dt = fields.Datetime.from_string(date_from)
                to_dt = fields.Datetime.from_string(date_to)
                time_delta = to_dt - from_dt
                total_hours = time_delta.days * 24 + float(time_delta.seconds) / 3600
                self.number_of_hours_temp = total_hours
                self.number_of_days_temp = total_hours / 8


class HrHolidaysLeft(models.Model):
    _inherit = "hr.holidays.left"

    enjoyed_time = fields.Float(string='Эдэлсэн цаг', readonly=True)

    def init(self):
        tools.sql.drop_view_if_exists(self.env.cr, 'hr_holidays_left')
        self.env.cr.execute("""
            CREATE or REPLACE view hr_holidays_left as 
            (SELECT row_number() OVER () AS id, a.employee_id as employee_id, a.department_id as department_id, a.holiday_status_id as holiday_status_id, 
            a.job_id as job_id, SUM(a.allocated_time)-(SUM(a.taken_time)-SUM(a.allocated_time)) as left_time, SUM(a.allocated_time) as allocated_time, SUM(a.taken_time)-SUM(a.allocated_time) as enjoyed_time
            FROM (SELECT h.id as id, h.type, e.id as employee_id, e.department_id as department_id, e.job_id as job_id, s.id as holiday_status_id,
             CASE WHEN h.type='add' AND h.state='validate' 
             THEN sum(COALESCE(h.number_of_hours_temp,0)) WHEN h.type='remove' 
             THEN sum(COALESCE(h.number_of_hours_temp,0)+(h.number_of_days_temp)*COALESCE(c.work_hours_day, 8)) ELSE 0 END AS taken_time, 
             CASE WHEN h.type='add' AND h.state='validate' 
             THEN sum(COALESCE(h.number_of_hours_temp,0)) ELSE 0 END AS allocated_time 
             FROM hr_holidays h LEFT JOIN hr_holidays_status s on h.holiday_status_id = s.id 
             LEFT JOIN hr_employee e on h.employee_id=e.id 
             LEFT JOIN resource_resource r on e.resource_id=r.id 
             LEFT JOIN res_users u on r.user_id=u.id 
             LEFT JOIN resource_calendar c ON r.calendar_id = c.id 
             WHERE s.limit = 'f' and u.active='t' and h.state='validate'  
             GROUP BY h.id, e.id,s.id,s.name, h.type, h.state) AS a GROUP BY a.employee_id, a.department_id, a.holiday_status_id, a.job_id 
             HAVING SUM(a.allocated_time)-(SUM(a.taken_time)-SUM(a.allocated_time)) >0 
             AND SUM(a.allocated_time)>=SUM(a.allocated_time)-(SUM(a.taken_time)-SUM(a.allocated_time)));
             
             CREATE OR REPLACE RULE update_hr_holidays_left AS
              ON UPDATE TO hr_holidays_left 
              DO INSTEAD
              UPDATE hr_employee SET job_id = new.job_id 
              WHERE hr_employee.id = old.id;
        """)

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            name = "%(name)s (%(count)s)" % {
                'name': record.holiday_status_id.name,
                'count': _('%g remaining out of %g') % (
                 record.left_time or 0.0, record.allocated_time or 0.0)
            }
            res.append((record.id, name))
        return res