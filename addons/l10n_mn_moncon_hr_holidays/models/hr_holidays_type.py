# -*- coding: utf-8 -*-

from odoo import _, api, fields, models

class HolidaysType(models.Model):

    _inherit = "hr.holidays.status"

    is_nohon_amrakh = fields.Boolean(u'Нөхөн амрах')
