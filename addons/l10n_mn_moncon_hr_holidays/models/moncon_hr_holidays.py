# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError
from odoo import tools
from datetime import timedelta, datetime
import pytz

class MonconHrHolidays(models.Model):
    _name = "moncon.hr.holidays"
    _inherit = 'mail.thread'

    name = fields.Char(u'Нэр')
    type = fields.Selection([('add', u'Нөхөн амрах хоног хуримтлуулах'),
                             ('remove', u'Нөхөн амрах хоног биеэр эдлэх')], default='add')
    date_from = fields.Datetime(u'Эхлэх өдөр')
    date_to = fields.Datetime(u'Дуусах өдөр')
    days = fields.Float(u'Өдөр')
    hours = fields.Float(u'Цаг')
    state = fields.Selection([('draft', u'Ноорог'),
                              ('send', u'Хүсэлт илгээгдсэн'),
                              ('approved', u'Зөвшөөрсөн'),
                              ('canceled', u'Хүсэлт цуцлагдсан'),
                              ('accepted', u'Бүртгэгдсэн')
                              ], 'Төлөв', default='draft')
    comment = fields.Char(u'Тайлбар')
    check_in = fields.Datetime(string="Check In", compute='_compute_check_in')
    check_out = fields.Datetime(string="Check Out", compute='_compute_check_in')
    employee_id = fields.Many2one('hr.employee', 'Ажилтан', required =True)

    @api.multi
    @api.onchange('date_from')
    def _compute_check_in(self):
        for obj in self:
            if obj.date_from:
                start = datetime.strptime(obj.date_from, '%Y-%m-%d %H:%M:%S').date()
                attendances = self.env['hr.attendance.download'].get_attendances_of_day(obj.employee_id, start)
                for attendance in attendances:
                    obj.check_in = attendance.check_in
                    obj.check_out = attendance.check_out

    history_lines = fields.One2many('moncon.hr.holidays.workflow.history', 'history', 'Workflow History')


class ExpenseWorkflowHistory(models.Model):
    _name = 'moncon.hr.holidays.workflow.history'
    _order = 'history, sent_date'

    STATE_SELECTION = [('waiting', 'Waiting'),
                       ('confirmed', 'Confirmed'),
                       ('approved', 'Approved'),
                       ('return', 'Return'),
                       ('rejected', 'Rejected')]
    history = fields.Many2one('moncon.hr.holidays', 'Expense', readonly=True, ondelete='cascade')
    line_sequence = fields.Integer('Workflow Step')
    name = fields.Char('Verification Step', readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_holidays_workflow_history_ref', 'history_id', 'user_id', 'Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)