# -*- coding: utf-8 -*-
# Part of Asterisk technologies. See LICENSE file for full copyright and licensing details.
{

    'name': 'Moncon - base module',
    'version': '1.0',
    'author': 'Moncon LLC',
    "description": """
     Уг модуль нь Монконы ерөнхий суурь модуль болно.
 """,
    'website': 'http://www.moncon.erp.mn',
    'images': [''],
    'depends': ['l10n_mn_base'],
    'data': [
        'security/security_users.xml'
    ],
    'installable': True,
    'auto_install': False
}
