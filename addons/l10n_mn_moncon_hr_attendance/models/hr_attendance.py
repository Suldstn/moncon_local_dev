# -*- coding: utf-8 -*-
import logging
from datetime import datetime, timedelta

import pytz
from dateutil import rrule
from dateutil.relativedelta import relativedelta

from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class HrAttendance(models.Model):
    _inherit = "hr.attendance"

    work_end_hour_times = fields.Datetime('Илүү цаг тооцох цаг')
    hr_holidays_id = fields.Many2one('hr.holidays', string=u'Амралт чөлөө')
    holidays_type = fields.Char('Амралт чөлөөний', compute='_holidays_type')

    @api.multi
    def _holidays_type(self):
        for obj in self:
            local = pytz.timezone(self.env.user.tz)
            date_time_obj = pytz.utc.localize(datetime.strptime(obj.check_in, '%Y-%m-%d %H:%M:%S')).astimezone(local)
            date = date_time_obj.date()
            holidays = self.env['hr.holidays'].search([('employee_id', '=', obj.employee_id.id), ('date_from', '>=', str(date) + ' 00:00:00'), ('date_from', '<=', str(date) + ' 23:59:59')])
            holiday = ''
            for hol in holidays:
                holiday = hol.holiday_status_id.name +'- '+ str(hol.number_of_days_temp) + u'-өдөр ' + str(hol.number_of_hours_temp)+ u'-цаг'
            
            rapair_attendances = self.env['hr.attendance.repair'].search([('employee_id', '=', obj.employee_id.id), ('date_of_attendance', '>=', str(date) + ' 00:00:00'), ('date_of_attendance', '<=', str(date) + ' 23:59:59')])
            repair_attendance = ''
            for repair in rapair_attendances:
                repair_attendance = u'Ирц засах огноо- ' +str(repair.date_of_attendance) + '  '
            obj.holidays_type = repair_attendance + holiday

    @api.multi
    def _hr_holidays_count(self):
        for obj in self:
            hr_holidays_ids = self.env['hr.holidays'].search([('id', '=', obj.hr_holidays_id.id)])
            obj.hr_holidays_count = len(hr_holidays_ids)
    
    hr_holidays_count = fields.Integer(string='Нийт нөхөж амрах хоног', compute='_hr_holidays_count')
     

    @api.multi
    def stat_button_hr_holidays(self):
        for obj in self:
            res = self.env['hr.holidays'].search([('id', '=', obj.hr_holidays_id.id)])
            return {
                'type': 'ir.actions.act_window',
                'name': u'Нөхөж амрах хоног',
                'res_model': 'hr.holidays',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }

    @api.model
    def create(self, vals):
        res = super(HrAttendance, self).create(vals)
        detail = self.env['hr.attendance.detail'].search([('report_datetime', '=', res.check_in)])
        if not detail:
            self.env['hr.attendance.detail'].create({'report_datetime': res.check_in, 'name': 'Attendance'})
        return res

    # @api.multi
    # def input_holidays(self):
    #     for attendance in self:
    #         if attendance.hr_holidays_id:
    #             raise ValidationError(u'Нөхөж амрах хоног үүссэн байна!!!')
    #         elif not attendance.over_time_hours:
    #             raise ValidationError(u'Илүү цаг байхгүй байна!!!')
    #         else:
    #             holidays_obj = self.env['hr.holidays']
    #             holiday_status_ids = self.env['hr.holidays.status'].search([('balance_type','=','paid'),('limit', '!=', True)], limit=1)
    #             vals = {
    #                 "name": u'Ирцээс илүү цагаас үүссэн нөхөн амрах хоног',
    #                 "type": "add",
    #                 "holiday_status_id": holiday_status_ids.id,
    #                 "number_of_hours_temp": attendance.over_time_hours,
    #                 "employee_id": attendance.employee_id.id,
    #                 "department_id": attendance.employee_id.department_id.id,
    #                 "holiday_type": "employee"
    #             }
    #             holidays_id = holidays_obj.create(vals)
    #             attendance.hr_holidays_id = holidays_id

    @api.multi
    def _set_workable_hours(self):
        for obj in self:
            if obj.working_hours:
                check_in_day = change_date_to_user_tz(obj.check_in, self.env.user).replace(tzinfo=None)
                attends = obj.working_hours.attendance_ids.filtered(lambda att: att.dayofweek == str(check_in_day.weekday()))

                # Ажил эхлэх болон дуусах цагийг тооцох
                if attends:
                    obj.work_start_hour = attends.sorted(key='hour_from')[0].hour_from
                    obj.work_end_hour = attends.sorted(key='hour_to', reverse=True)[0].hour_to
                    if not obj.check_attendance_day_is_not_work_day():
                        obj.work_start_time = get_day_to_display_timezone_from_floattime(obj.check_in, obj.work_start_hour, self.env.user)
                        obj.work_end_hour_times = get_day_to_display_timezone_from_floattime(obj.check_in, obj.work_end_hour +4 , self.env.user)
                        obj.work_end_time = get_day_to_display_timezone_from_floattime(obj.check_in, obj.work_end_hour, self.env.user)
                    else:
                        obj.work_start_time = False
                        obj.work_end_time = False
                else:
                    obj.work_start_hour = 0
                    obj.work_end_hour = 0

                # Ажиллах цаг тооцох
                if obj.check_attendance_day_is_not_work_day():
                    obj.work_hour_of_date = 0
                elif obj.work_start_time and obj.work_end_time:
                    end_date = get_day_by_user_timezone(obj.work_end_time, self.env.user)
                    start_date = get_day_by_user_timezone(obj.work_start_time, self.env.user)
                    obj.work_hour_of_date = obj.working_hours.get_working_hours_of_date(start_dt=start_date,end_dt=end_date)
                else:
                    obj.work_hour_of_date = 0

    @api.multi
    def _get_over_time_hours(self):
        self.ensure_one()
        over_time_hours = 0

        if not self.check_attendance_day_is_not_work_day():
            if self.work_end_hour_times and self.check_out > self.work_end_hour_times:
                gross_minutes = get_difference_btwn_2date(self.work_end_hour_times, self.check_out, {'diff_type': 'hour'})
                paid_holiday, unpaid_holiday, annual_leave, sick_leave = self.get_duplicated_holiday_hours(self.work_end_hour_times, self.check_out, self.employee_id)
                holiday_minutes = (paid_holiday + unpaid_holiday + annual_leave + sick_leave) * 60
                over_time_hours = gross_minutes - holiday_minutes if (gross_minutes - holiday_minutes) > 0 else 0
            else:
                over_time_hours = 0

        return over_time_hours