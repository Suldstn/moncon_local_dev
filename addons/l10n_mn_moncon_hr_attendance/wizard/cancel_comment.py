# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
from odoo import api, fields, models, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from dateutil.relativedelta import relativedelta

class CancelCommnet(models.TransientModel):
    _name = 'cancel.comment'
    _description = u'Цуцлах тайлбар'

    comment = fields.Char('Тайлбар', required=True)

    def cancel_commnet(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        for line in self.env['hr.attendance.repair'].browse(active_ids):
            if line.type_of_attendance == 'in':
                attendance_ids = self.env['hr.attendance'].search([('employee_id', '=', line.employee_id.id), ('check_in', '=', line.date_of_attendance)])
                for attendance_id in attendance_ids.filtered(lambda x: x.check_out):
                    attendance_id.write({'check_in': datetime.strptime(attendance_id.check_out, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=-1)})
                attendance_ids.filtered(lambda x: not x.check_out).unlink()
            else:
                attendance_ids = self.env['hr.attendance'].search([('employee_id', '=', line.employee_id.id), ('check_out', '=', line.date_of_attendance)])
                for attendance_id in attendance_ids.filtered(lambda x: x.check_in):
                    if attendance_id.check_in:
                        attendance_id.write({'check_out': datetime.strptime(attendance_id.check_in, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=1)})
                attendance_ids.filtered(lambda x: not x.check_in).unlink()
            line.cancel_comment = self.comment
            line.write({'state': 'canceled'})