# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
from os import path
import time
from datetime import timedelta
from datetime import datetime
from dateutil import parser
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _  # @UnresolvedImport.
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport
from odoo.addons.l10n_mn_web.models.time_helper import *

class MonconHrEmployee(models.Model):
    _inherit = "hr.employee"

    employee_type = [
        ('employee_office', u'Оффис'),
        ('employee_project', u'Төсөл')
    ]

    employee_type = fields.Selection(employee_type, string=u'Ангилал')