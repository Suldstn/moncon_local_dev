# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *

employee_type = [
    ('employee_office', u'Оффис'),
    ('employee_project', u'Төсөл')
]

class HourBalance(models.Model):
    _inherit = "hour.balance"

    employee_type = fields.Selection(employee_type, string=u'Ангилал', required=True, default='employee_office')

    def get_available_employees(self):
        employee_obj = self.env['hr.employee'].sudo()
        employees = []

        for obj in self:
            # 'Урьдчилгаа цалин' төрөлтэй цагийн баланс үед 'Жирэмсэн' болон 'Ажлаас гарсан' төлөвтэй ажилчдыг НЭМЭХГҮЙ байх
            ignored_states = ['resigned', 'maternity'] if obj.salary_type == 'advance_salary' else ['resigned']
            ignored_statuses = self.env['hr.employee.status'].sudo().search([('type', 'in', ignored_states)])

            # Update of balance sheet information
            domain = [('active', '=', True), ('company_id', '=', obj.company_id.id)]
            if ignored_statuses and len(ignored_statuses) > 0:
                domain.append(('state_id', 'not in', ignored_statuses.sudo().ids))
            if obj.department_id:
                domain.append(('department_id', 'child_of', [obj.department_id.id]))
            if obj.employee_type:
                domain.append(('employee_type', '=', obj.employee_type))
            employees.extend(employee_obj.search(domain))
        return employees

class HourBalanceLine(models.Model):
    _inherit = "hour.balance.line"

    rehabilitated_holiday = fields.Float(string='Нөхөн амраасан цаг', digits=(4, 2))
    attendance_repair_count = fields.Integer(compute="_attendance_repair_count")
    hr_holidays_count = fields.Integer(compute='_hr_holidays_count')

    def _attendance_repair_count(self):
        attendanc_repair_ids = self.env['hr.attendance.repair'].search([('employee_id', '=', self.employee_id.id), ('date_of_attendance', '>=', self.balance_id.balance_date_from), ('date_of_attendance', '<=', self.balance_id.balance_date_to)])
        count = len(attendanc_repair_ids)
        self.attendance_repair_count = count

    def _hr_holidays_count(self):
        hr_holidays_ids = self.env['hr.holidays'].search([('employee_id', '=', self.employee_id.id), ('date_from', '>=', self.balance_id.balance_date_from),
                                                                        ('date_from', '<=', self.balance_id.balance_date_to)])
        count = len(hr_holidays_ids)
        self.hr_holidays_count = count

    def check_hr_holidays(self):
        hr_holidays_ids = self.env['hr.holidays'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('date_from', '>=', self.balance_id.balance_date_from),
             ('date_from', '<=', self.balance_id.balance_date_to)])

        tree_id = self.env.ref("l10n_mn_hr_hour_balance.view_holiday_employee_inherit")

        return {
            'type': 'ir.actions.act_window',
            'name': u'Амралт чөлөө',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.holidays',
            'domain': [('id', 'in', hr_holidays_ids.ids)],
            'views': [(tree_id.id, 'tree')],
            'target': 'current'
        }

    def confirm(self):
        self.sudo().state = 'sent'

    def check_attendance(self):
        attendanc_repair_ids = self.env['hr.attendance.repair'].sudo().search(
            [('employee_id', '=', self.employee_id.id), ('date_of_attendance', '>=', self.balance_id.balance_date_from),
             ('date_of_attendance', '<=', self.balance_id.balance_date_to)])

        tree_id = self.env.ref("l10n_mn_hr_attendance_repair.view_attendance_repair_tree")

        return {
            'type': 'ir.actions.act_window',
            'name': u'Ирц нөхөн засвар',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.attendance.repair',
            'domain': [('id', 'in', attendanc_repair_ids.ids)],
            'views': [(tree_id.id, 'tree')],
            'target': 'current'
        }


    def update_line(self):
        if self.company_id.hr_contract_number == 1:
            contract = self.employee_id.contract_id
            if contract and contract.active:
                values = self.balance_id.get_balance_line_values(self.employee_id, contract)
                if self.balance_id.can_insert_balance_line(self.employee_id, contract, self.balance_id.get_active_hours(values)):
                    self.sudo().write(values)

