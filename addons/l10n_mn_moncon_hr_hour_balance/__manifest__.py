# -*- coding: utf-8 -*-

{
    'name': "Moncon HR - Hour balance",
    'version': '1.0',
    'depends': [
        'l10n_mn_hr_hour_balance',
    ],
    'author': "Moncon LLC",
    'website': 'http://moncon.erp.mn',
    'category': 'Moncon Modules',
    'description': """
      Employee Hour balance
    """,
    'data': [
        'security/balance_security.xml',
        'views/hour_balance_view.xml',
        'views/moncon_hr_employee_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
