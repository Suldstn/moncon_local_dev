# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from odoo.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class HrRegulation(models.Model):
    _inherit = 'hr.regulation'

    type_category = fields.Selection(related='regulation_type_id.category', string=u'Тушаалын утга', readonly=True)
    employee_ids = fields.Many2many('hr.employee', 'hr_regulation_employee_rel', 'regulation_id', 'employee_id', u'Ажилчид')
    general_end_date = fields.Date('End Date of Implementation')
    compute_employees = fields.Boolean(compute='_compute_employees')

    @api.multi
    @api.onchange('employee_ids', 'employee_ids')
    def _compute_employees(self):
        for obj in self:
            if not self.employee_ids and self.employees:
                obj.write({'employee_ids': [[6, False, self.employees.mapped('employee').ids]]})
            obj.compute_employees = False

    @api.model
    def create(self, vals):
        if vals.get('employee_ids'):
            vals['employees'] = []
            for obj in vals['employee_ids'][0][2]:
                vals['employees'].append([0, False, {'employee': obj}])
        res = super(HrRegulation, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        res = super(HrRegulation, self).write(vals)
        for obj in self:
            obj.employees.filtered(lambda l: l.employee not in obj.employee_ids).unlink()
        return res

    @api.one
    def action_to_confirm(self):
        self.send_notification()
        if self.regulation_type_id.category == 'b':
            self.write({'reg_number': self.env['ir.sequence'].next_by_code('hr.regulation')})
        else:
            self.write({'reg_number': self.env['ir.sequence'].next_by_code('hr.regulation.a')})
        return self.write({'state': 'confirmed'})

class HrRegulationEmployee(models.Model):
    _inherit = 'hr.regulation.employee'

    type_category = fields.Selection(related='regulation.type_category', string=u'Тушаалын утга', readonly=True, store=True)

    @api.constrains('start_date', 'end_date')
    def check_start_end_date(self):
        for obj in self:
            if obj.regulation and obj.regulation.general_start_date and obj.regulation.general_start_date > obj.start_date:
                raise ValidationError(_("%s Employee regulation start date must be after %s ") %
                                      (obj.employee.name, obj.regulation.general_start_date))
            if obj.regulation and obj.regulation.general_end_date and obj.regulation.general_end_date < obj.end_date:
                raise ValidationError(_("%s Employee regulation end date must be before %s ") %
                                      (obj.employee.name, obj.regulation.general_end_date))

    @api.model
    def create(self, vals):
        if vals.get('regulation'):
            regulation = self.env['hr.regulation'].browse(vals['regulation'])
            if regulation.general_start_date and not vals.get('start_date'):
                vals['start_date'] = regulation.general_start_date
            if regulation.general_end_date and not vals.get('end_date'):
                vals['end_date'] = regulation.general_end_date
        return super(HrRegulationEmployee, self).create(vals)

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.regulation:
                query = '''
                    DELETE FROM hr_regulation_employee_rel
                    WHERE regulation_id = %s and employee_id = %s
                ''' % (obj.regulation.id, obj.employee.id)
                self.env.cr.execute(query)
        return super(HrRegulationEmployee, self).unlink()
