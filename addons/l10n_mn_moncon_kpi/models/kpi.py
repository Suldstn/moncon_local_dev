# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import date, datetime, timedelta
from odoo.exceptions import UserError, ValidationError
from odoo.http import request
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo import _


class Kpi(models.Model):

    _name = 'moncon.kpi'
    _description = 'KPI'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name_seq = fields.Char(string='Дугаар', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    name = fields.Char(string='Name', size=256, track_visibility='onchange')
    period_id = fields.Many2one('account.period', string="KPI Month", domain=[('state', 'not in', ['done'])], track_visibility='onchange')
    kpi_state = fields.Selection([('draft', 'New'), ('done', 'Approved'), ], string='Төлөв', default='draft')
    kpi_lines = fields.One2many('moncon.kpi.line', 'kpi_line_id', string='Kpi employee lines')


    participant_employee_ids = fields.Many2many('hr.employee', string="Participating Employees",
                                                domain="[('state_id.type','not in',('maternity','resigned', 'retired'))]")

    def action_send_mail_kpi_valuation(self):
        for kpi in self:
            group_kpi_manager = self.env.ref('l10n_mn_moncon_kpi.group_kpi_manager').id
            user_ids = self.env['res.users'].search([('groups_id', 'in', [group_kpi_manager])])
            model_obj = self.env['ir.model.data']
            ctx = {
                'name': kpi.name,
                'month': kpi.period_id.name,
                'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
                'action_id': model_obj.get_object_reference('l10n_mn_moncon_kpi', 'action_moncon_kpi_tree_view')[1],
                'id': kpi.id,
                'db_name': request.session.db,
                'sender': self.env['res.users'].search([('id', '=', self._uid)]).name,
            }

            template_id = self.env.ref('l10n_mn_moncon_kpi.action_send_kpi_email_template')

            if user_ids:
                user_emails = []
                for user in user_ids:
                    user_emails.append(user.login)
                    template_id.with_context(ctx).send_mail(user.id, force_send=True)
                email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
                        '<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
                kpi.message_post(body=email)


    #KPI мөрийн ажилтны төлөв батлагдсан биш байвал мэссэж өгөх.
    def finish_kpi_done(self):
        for rec in self:
            if rec.kpi_lines:
                for line in rec.kpi_lines:
                    if line.kpi_line_state1 != 'done' or line.kpi_line_state2 != 'done':
                        raise ValidationError(_('Error ! Worker state is not done!!!.'))
                    else:
                        False
            else:
                 False
            rec.kpi_state = 'done'

    def cancel_kpi_draft(self):
        for rec in self:
            rec.kpi_state = 'draft'

    @api.model
    def create(self, vals):
        if vals.get('name_seq', _('New')) == _('New'):
            vals['name_seq'] = self.env['ir.sequence'].next_by_code('kpi.sequence') or _('New')
        result = super(Kpi, self).create(vals)
        return result

    @api.multi
    def add_employees(self):
        this_object = self.env['moncon.kpi'].search([('id', '=', self._context.get('active_id'))])
        kpi_line = self.env.get('moncon.kpi.line').search([('kpi_line_id', '=', this_object.id)])
        # Оролцогчдын жагсаалтыг авч байна
        kpi_line_employee_ids = []
        for line_employee in kpi_line:
            kpi_line_employee_ids.append(line_employee.employee_id.id)

        emp_id = this_object.participant_employee_ids



        for line in emp_id:
            # Сонгосон ажилчид оролцсон эсэхийг шалгаж байхгүй бол нэмнэ.
            if line.id not in kpi_line_employee_ids:
                kpi_line.create({'employee_id': line.id,
                                 'kpi_line_id': this_object.id,
                                 'kpi_percent_weight1': line.kpi_percent_weight1,
                                 'approve_user_id1': line.approve_user_id1.id,
                                 'kpi_percent_weight2': line.kpi_percent_weight2,
                                 'approve_user_id2': line.approve_user_id2.id,
                                 'period_id': this_object.period_id.id,
                                 'kpi_line_state1': 'done' if not line.approve_user_id1.id else 'draft',
                                 'kpi_line_state2': 'done' if not line.approve_user_id2.id else 'draft'})
            this_object.participant_employee_ids = False

    def add_participates_view(self):
        for obj in self:
            view = obj.env.ref('l10n_mn_moncon_kpi.add_employee_view')
            return {
                'name': _('Add participants'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'moncon.kpi',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': self.id
            }



class AttitudeType(models.Model):
    _name = 'moncon.kpi.line'
    _rec_name = 'employee_id'

    kpi_line_id = fields.Many2one('moncon.kpi', string='KPI id', ondelete='cascade')
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True, index=True)
    employee_name = fields.Char(related="employee_id.name")
    employee_job_name = fields.Char(related="employee_id.job_id.name")
    period_id = fields.Many2one('account.period', string="KPI line month", related='kpi_line_id.period_id')
    kpi_percent_weight1 = fields.Float(string='KPI Weight 1', digits=(3, 2), default=0)
    kpi_first_value = fields.Float(string='KPI Value 1', digits=(3, 2), default=0)
    kpi_note1 = fields.Char(string='Note', size=80)
    approve_user_id1 = fields.Many2one('res.users', string='User1 to approve',)
    kpi_line_state1 = fields.Selection([('draft', 'New'), ('done', 'Approved')], string='Төлөв',)
    kpi_percent_weight2 = fields.Float(string='KPI Weight 2', digits=(3, 2), default=0)
    kpi_second_value = fields.Float(string='KPI Value 2', digits=(3, 2), default=0)
    kpi_note2 = fields.Char(string='Note', size=80)
    approve_user_id2 = fields.Many2one('res.users', string='User2 to approve',)
    total_kpi = fields.Float(string='Total KPI', compute='_compute_total_kpi')
    kpi_line_state2 = fields.Selection([('draft', 'New'), ('done', 'Approved')], string='Төлөв', default='draft')
    show_approve_button1 = fields.Boolean(string='Show Approve Button1?', compute='_show_approve_button1')
    show_approve_button2 = fields.Boolean(string='Show Approve Button2?', compute='_show_approve_button2')
    show_kpi_manager = fields.Boolean(string='Show KPI Manager?', compute='_show_kpi_manager')
    kpi_line_state_form = fields.Selection([('draft', 'New'), ('done', 'Approved'),], readonly=True, related='kpi_line_id.kpi_state', string='State related')


    def to_approve_employee_kpi1(self):
        for line in self:
            line.kpi_line_state1 = 'done'

    def to_cancel_employee_kpi1(self):
        for line in self:
            line.kpi_line_state1 = 'draft'

    def to_approve_employee_kpi2(self):
        for line in self:
            line.kpi_line_state2 = 'done'

    def to_cancel_employee_kpi2(self):
        for line in self:
            line.kpi_line_state2 = 'draft'

    #KPI Үнэлгээ 100- аас их, 0-ээс бага байвал хадгалах үед анхааруулга өгөх.
    @api.constrains('kpi_first_value', 'kpi_second_value')
    def _check_kpi_value_percent(self):
        for line in self:
            if line.kpi_first_value > 100 or line.kpi_first_value < 0:
                raise ValidationError(_('Error ! Percent must be between 0 to 100!!!.'))
            elif line.kpi_second_value > 100 or line.kpi_second_value < 0:
                raise ValidationError(_('Error ! Percent must be between 0 to 100!!!.'))
            else:
                return False

    #Нийт KPI Жингүүдээр тооцоолох функц
    @api.depends('kpi_first_value', 'kpi_second_value', 'kpi_percent_weight1', 'kpi_percent_weight2')
    @api.multi
    def _compute_total_kpi(self):
        for line in self:
            if 0 < line.kpi_first_value <= 100 or 0 < line.kpi_second_value <= 100:
                line.total_kpi = ((line.kpi_percent_weight1 * line.kpi_first_value)/100) + ((line.kpi_percent_weight2 * line.kpi_second_value) / 100)

    #Батлах ажилтанд өөрийн талбаруудыг харуулах функц
    @api.multi
    def _show_approve_button1(self):
        current_emp = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        for line in self:
            if line.approve_user_id1 == current_emp.user_id:
                line.show_approve_button1 = True
            else:
                line.show_approve_button1 = False
    #Батлах ажилтанд өөрийн талбаруудыг харуулах функц2
    @api.multi
    def _show_approve_button2(self):
        current_emp = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        for line in self:
            if line.approve_user_id2 == current_emp.user_id:
                line.show_approve_button2 = True
            else:
                line.show_approve_button2 = False

    #KPI менежер бүгдийг харах, засах эрхтэй хэрэглэгч
    @api.multi
    def _show_kpi_manager(self):
        current_emp = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        group_kpi_manager = self.env.ref('l10n_mn_moncon_kpi.group_kpi_hr_manager').id
        user_ids = self.env['res.users'].search([('groups_id', 'in', [group_kpi_manager])])
        for obj in self:
            if current_emp.user_id in user_ids:
                obj.show_kpi_manager = True
            else:
                obj.show_kpi_manager = False






