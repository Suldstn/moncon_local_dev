# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from odoo.exceptions import ValidationError
from odoo import exceptions
import time
import logging
import datetime
from dateutil.relativedelta import relativedelta

_logger = logging.getLogger(__name__)


class Contract(models.Model):
    _inherit = 'hr.contract'
    field_addition = fields.Boolean('Field additon', help="If field addition give to employee, please check it")
    country_addition = fields.Boolean('Country additon', help="If country addition give to employee, please check it")
    project_leading_addition = fields.Float('Project leading addition', help="If project leading addition give to employee, please check it")
    kpi_salary_percent = fields.Float('KPI Percent', help="Write KPI percent")
    kpi_salary = fields.Float('KPI Salary', compute="_kpi_salary_amount")

    # Нийт KPI Жингүүдээр тооцоолох функц
    @api.depends('kpi_salary_percent')
    @api.multi
    def _kpi_salary_amount(self):
        for obj in self:
            if obj.kpi_salary_percent:
                obj.kpi_salary = (obj.wage * obj.kpi_salary_percent)/100
            else:
                return False

    #KPI Хувь 100- аас их, 0-ээс бага байвал хадгалах үед анхааруулга өгөх.
    @api.constrains('kpi_salary_percent')
    def _check_kpi_percent(self):
        for obj in self:
            if obj.kpi_salary_percent > 100 or obj.kpi_salary_percent < 0:
                raise ValidationError(_('Error ! Percent must be between 0 to 100!!!.'))
            else:
                return False




