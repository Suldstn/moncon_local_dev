# -*- coding: utf-8 -*-
# Part of Asterisk technologies. See LICENSE file for full copyright and licensing details.
{

    'name': 'Moncon - Document module',
    'version': '1.0',
    'author': 'Moncon LLC',
    "description": """
     Уг модуль нь Монконы ерөнхий бичиг баримтын модуль болно.
 """,
    'website': 'http://www.moncon.erp.mn',
    'images': [''],
    'depends': ['l10n_mn_document'],
    'data': [
        'views/document_type_view.xml',
        'views/document_view.xml',
        'views/sequence_view.xml'
    ],
    'installable': True,
    'auto_install': False
}
